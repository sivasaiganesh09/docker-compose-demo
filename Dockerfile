FROM golang:alpine3.14
RUN mkdir /app
WORKDIR /app
COPY . .
#RUN go mod init
RUN go env -w GO111MODULE=off
RUN go build -o main .
CMD ["/app/main"]


