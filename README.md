**Prerequisite:** 

1. Docker Desktop or minikube


## Build GO application using dockerfile:

1. Clone the repository into your local directory.
2. Change the directory path to docker-compose-demo.
3. Run the command `docker build -t <tag-name> .`
4. Run the command `docker run -itd -p 3000:3000 <Image-id>`
5. To access the application, Enter http://localhost:3000 in browser URL

## Build GO application using docker-compose:

1. Clone the repository into your local directory.
2. Change the directory path to docker-compose-demo.
3. Run the command `docker-compose up -d --build`
4. To access the application, Enter http://localhost:3000 in browser URL

## To run the application in kubernetes:

1. Enable kubernetes in docker desktop settings.
2. Clone the repository into your local directory.
3. Change the directory path to docker-compose-demo.
4. Run the command `kubectl apply -f goapp-deployment.yaml`. After this step you can access the application using 127.0.0.1:30010
5. Configure the hostname in C:\Windows\System32\drivers\etc\hosts file(eg: 127.0.0.1 goapp.com) to access the application using domain name
6. To access the application, Enter http://goapp.com:30010 in the browser URL.

Note: Attached the screenshot(kubernetes-deployment-screenshot) for reference. 

## To run the application in kubernetes and access it using HTTPS:

1. Follow the same steps as above from 1 to 3 from above.
2. Run the command `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.2/deploy/static/provider/cloud/deploy.yaml` to install nginx ingress controller.
3. Download openssl and install it using this link `https://slproweb.com/products/Win32OpenSSL.html`
4. Run the command `openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout tls.key -out tls.crt -subj "/CN=goapp.com" -days 365`
5. Run the command `kubectl create secret tls goapp-com-tls --cert=tls.crt --key=tls.key`
6. Run the command `kubectl apply -f goapp-ingress.yaml`
7. Configure the hostname in C:\Windows\System32\drivers\etc\hosts file(eg: 127.0.0.1 goapp.com) to access the application using domain name
8. To access the application, Enter https://goapp.com in the browser URL.

Note: Attached the screenshot(kubernetes-ingress-https-screenshot) for reference.








